# Codaxy Test Netflix Conductor

## Run Netflix Conductor Server
```CLI
docker-compose up
```
For detailed explanation on how things work, checkout the [Docker Documentation](https://docs.docker.com/)

## Netflix conductor Links
- [Conductor Swagger documentation](http://localhost:8080)
- [Conductor UI](http://localhost:8080)

For detailed explanation on how things work, checkout the [Conductor Documentation](https://netflix.github.io/conductor/)

## Build Setup

``` CLI
# install dependencies
dotnet restore

#build application
dotnet build

# run app
cd CodacyText
dotnet run
```

For detailed explanation on how things work, checkout the [Microsoft Documentation](https://docs.microsoft.com/en-us/dotnet/fundamentals)
