﻿using CodaxyTest.Services.NetflixConductor;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodaxyTest
{
    class Program
    {
        
        static async Task Main(string[] args)
        {
            bool exitApp = false;
            Console.WriteLine("Start Program");
            
            ConductorService conductorService = new ConductorService();
            await conductorService.WorkflowInitialization();
            conductorService.StartWorkers();

            Console.WriteLine("Press:");
            Console.WriteLine("0 - Exit Program");
            Console.WriteLine("Enter contentId:");

            do
            {
                var consoleInput = Console.ReadLine();

                switch (consoleInput.ToString())
                {
                    case "0":
                        exitApp = true;
                        break;

                    default:
                        
                        if (consoleInput.Length != 0)
                        {
                            var inputData = new Dictionary<string, object>() {
                                {"contentId", consoleInput }
                            };

                            await conductorService.StartWorkflow(inputData);
                            continue;
                        }

                        await conductorService.StartWorkflow();
                        break;
                }
            } while (!exitApp);







        }
    }
}
