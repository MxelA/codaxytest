﻿using CodaxyTest.Services.NetflixConductor.Models;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using ConductorDotnetClient.Extensions;
using ConductorDotnetClient;
using System;
using ConductorDotnetClient.Interfaces;
using System.Collections.ObjectModel;
using CodaxyTest.Services.NetflixConductor.Tasks;
using System.Collections.Generic;

namespace CodaxyTest.Services.NetflixConductor
{
    class ConductorService: IConductorService
    {
        protected readonly string _url = "http://localhost:8080/api/";
        protected readonly INetflixConductorCommunicationService _netflixConductorCommunicationService;
        protected NetflixConductorWorkflowDefinition _workflow;
        protected ServiceProvider _serviceProvider;
        public ConductorService(string conductorHost = null)
        {
            if(conductorHost != null)
            {
                _url = conductorHost;
            }

            _netflixConductorCommunicationService = new NetflixConductorCommunicationService(_url);
        }

        public void StartWorkers()
        {
            _serviceProvider = new ServiceCollection()
              //.AddLogging(p => p.AddConsole().SetMinimumLevel(LogLevel.Debug))
              .AddConductorWorkflowTask<VerifyIfIdentsAreAddedTask>()
              .AddConductorWorkflowTask<AddIdentsTask>()
              .AddConductorWorker(new ConductorClientSettings()
              {
                  ConcurrentWorkers = 1,
                  IntervalStrategy = ConductorClientSettings.IntervalStrategyType.Linear,
                  MaxSleepInterval = 15_000,
                  SleepInterval = 1_000,
                  ServerUrl = new Uri(_url)
              })
              .BuildServiceProvider();

            var workflowTaskCoordinator = _serviceProvider.GetRequiredService<IWorkflowTaskCoordinator>();
            foreach (var worker in _serviceProvider.GetServices<IWorkflowTask>())
            {
                workflowTaskCoordinator.RegisterWorker(worker);
            }

            workflowTaskCoordinator.Start();
        }

        public async Task WorkflowInitialization()
        {
            //CREATE TASKS
            var verifyIfIdentsAreAddedTask = new VerifyIfIdentsAreAddedTask();
            var addIdentsTask = new AddIdentsTask();

            Collection<IWorkflowTask> tasks = new Collection<IWorkflowTask>() {
                verifyIfIdentsAreAddedTask,
                addIdentsTask
            };    

            await _netflixConductorCommunicationService.CreateTask(tasks);

            //CREATE WORKFLOW
            _workflow = new NetflixConductorWorkflowDefinition()
            {
                name = "add_netflix_identation",
                description = "Adds Netflix Identation to video files.",
                version = 1,
                schemaVersion = 2,
                ownerEmail = "milicalex@gmail.com",
                tasks = new object[] {
                    new {
                        name = verifyIfIdentsAreAddedTask.name,
                        taskReferenceName = "ident_verification",
                        inputParameters = new
                        {
                            contentId ="${workflow.input.contentId}"
                        },
                        type = NetflixConductorWorkflowTaskDefinition.TYPE_SIMPLE

                    },
                     new {
                        name = "decide_task",
                        taskReferenceName = "is_idents_added",
                        type = NetflixConductorWorkflowTaskDefinition.TYPE_DECISION,
                        inputParameters = new
                        {
                            case_value_param ="${ident_verification.output.is_idents_added}"
                        },
                        caseValueParam = "case_value_param",
                        decisionCases = new
                        {
                            @false = new object[] {
                                new
                                {
                                    name = addIdentsTask.name,
                                    taskReferenceName = "add_idents_by_type",
                                    inputParameters = new {
                                        identType = "${workflow.input.identType}",
                                        contentId = "${workflow.input.contentId}",  
                                    },
                                    type = NetflixConductorWorkflowTaskDefinition.TYPE_SIMPLE
                                }
                            }
                        }
                    }
                }
            };

            await _netflixConductorCommunicationService.CreteWorkflow(_workflow);

        }

        public async Task<string> StartWorkflow(Dictionary<string,object> inputData = null)
        {
            var postData = new Dictionary<string, object>() {
                { "name", _workflow.name },
                { "version" , 1 },
                { "correlationId", "my_netflix_identation_workflows"}
            };

            if (inputData != null)
            {
                postData.Add("input", inputData);
            }
           
            return await _netflixConductorCommunicationService.StartWorkflow(postData);

        }
    }
}
