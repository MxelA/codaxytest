﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CodaxyTest.Services.NetflixConductor
{
    interface IConductorService
    {
        Task WorkflowInitialization();
        Task<string> StartWorkflow(Dictionary<string, object> workflowInputData = null);
        void StartWorkers();
    }
}
