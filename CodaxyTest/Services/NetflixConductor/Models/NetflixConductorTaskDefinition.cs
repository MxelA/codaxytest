﻿namespace CodaxyTest.Services.NetflixConductor.Models
{

    class NetflixConductorTaskDefinition
    {
        public static string RETRY_LOGIC_FIXED = "FIXED";
        public static string RETRY_LOGIC_EXPONENTIAL_BACKOFF = "EXPONENTIAL_BACKOFF";

        public static string TIME_OUT_POLICY_RETRAY = "RETRY";
        public static string TIME_OUT_POLICY_TIME_OUT_WF = "TIME_OUT_WF";
        public static string TIME_OUT_POLICY_ALERT_ONLY = "ALERT_ONLY";
    }
}
