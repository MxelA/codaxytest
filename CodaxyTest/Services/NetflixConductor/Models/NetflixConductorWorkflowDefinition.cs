﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodaxyTest.Services.NetflixConductor.Models
{
    class NetflixConductorWorkflowDefinition
    {
        public string name { get; set; }
        public string description { get; set; }
        public uint version { get; set; }
        public uint schemaVersion { get; set; }
        public string ownerEmail {get; set;}
        public object[] tasks { get; set; }
    }
}
