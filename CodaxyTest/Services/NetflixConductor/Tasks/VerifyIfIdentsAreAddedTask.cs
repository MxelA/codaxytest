﻿using ConductorDotnetClient.Interfaces;
using ConductorDotnetClient.Swagger.Api;
using System;
using ConductorDotnetClient.Extensions;
using System.Threading.Tasks;
using ConductorTask = ConductorDotnetClient.Swagger.Api.Task;
using Task = System.Threading.Tasks.Task;
using CodaxyTest.Services.NetflixConductor.Models;

namespace CodaxyTest.Services.NetflixConductor.Tasks
{

    class VerifyIfIdentsAreAddedTask : IWorkflowTask
    { 
        public string TaskType { get; set; } = "verify_if_idents_are_added";
        public int? Priority { get; set; } = 1;

        public string name { get {
                return TaskType;    
            } 
        }
        public ushort retryCount { get; } = 3;
        public string retryLogic { get; } = NetflixConductorTaskDefinition.RETRY_LOGIC_FIXED;
        public uint retryDelaySeconds { get; } = 10;
        public string timeoutPolicy { get; } = NetflixConductorTaskDefinition.TIME_OUT_POLICY_TIME_OUT_WF;
        public uint responseTimeoutSeconds { get; } = 180;
        public uint timeoutSeconds { get; } = 300;
        public string ownerEmail { get; } = "milicalex@gmail.com";
        public uint pollTimeoutSeconds { get; } = 0;

        public Task<TaskResult> Execute(ConductorTask task)
        {
            Console.WriteLine("Start work: " + name + " task");
           
            
            if( task.InputData["contentId"] == null)
            {
                var outputData = new System.Collections.Generic.Dictionary<string, object>() {
                    {"is_idents_added", false }
                };

                return Task.FromResult(task.Completed(outputData));
            }

            return Task.FromResult(task.Completed());
            
            //return Task.FromResult(task.Completed(new Dictionary<string, object>() { })); // with ouputdata
            //return Task.FromResult(task.Failed("error message ")); //error
            //return Task.FromResult(task.FailedWithTerminalError("error message")); // terminal failure
        }
    }
}
