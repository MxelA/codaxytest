﻿using CodaxyTest.Services.NetflixConductor.Models;
using ConductorDotnetClient.Interfaces;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CodaxyTest.Services.NetflixConductor
{
    class NetflixConductorCommunicationService: INetflixConductorCommunicationService
    {
        

        protected readonly string _url = "http://localhost:8080/api/";
        protected HttpClient _client;
        public NetflixConductorCommunicationService(string conductorHost = null)
        {
            if (conductorHost != null )
            {
                _url = conductorHost;
            }

            HttpClientHandler handler = new HttpClientHandler();
            _client = new HttpClient();
            _client.BaseAddress = new Uri(_url);
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<bool> CreateTask(IWorkflowTask netflixRequestDto)
        {
            var url = _url + "metadata/taskdefs";

            var jsonData = JsonConvert.SerializeObject(new[] { netflixRequestDto });
            var dataContent = new StringContent(jsonData, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await _client.PostAsync(url, dataContent);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Task is not created: " + response.Content.ReadAsStringAsync().Result);
            }

            return true;
        }

        public async Task<bool> CreateTask(Collection<IWorkflowTask> netflixRequestDtos)
        {
            var url = _url + "metadata/taskdefs";

            var jsonData = JsonConvert.SerializeObject(netflixRequestDtos.ToArray());
            var dataContent = new StringContent(jsonData, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await _client.PostAsync(url, dataContent);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Task is not created: " + response.Content.ReadAsStringAsync().Result);
            }

            return true;
        }

        public async Task<bool> CreteWorkflow(NetflixConductorWorkflowDefinition workflowDefinition)
        {
            var url = _url + "metadata/workflow";
            var jsonData = JsonConvert.SerializeObject(workflowDefinition);
            var dataContent = new StringContent(jsonData, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await _client.PostAsync(url, dataContent);

            if (!response.IsSuccessStatusCode && response.StatusCode != HttpStatusCode.Conflict)
            {
                throw new Exception("Workflow is not created: " + response.Content.ReadAsStringAsync().Result);
            }

            return true;
        }

        public async Task<string> StartWorkflow(object data)
        {
            var url = _url + "workflow";
            var jsonData = JsonConvert.SerializeObject(data);
            var dataContent = new StringContent(jsonData, Encoding.UTF8, "application/json");

            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
            HttpResponseMessage response = await _client.PostAsync(url, dataContent);

            if (!response.IsSuccessStatusCode)
            {

                throw new Exception("Workflow is not started: " + response.Content.ReadAsStringAsync().Result);
            }

            return response.Content.ReadAsStringAsync().Result;
        }
    }
}
