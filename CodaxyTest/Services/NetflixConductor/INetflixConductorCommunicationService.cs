﻿using CodaxyTest.Services.NetflixConductor.Models;
using ConductorDotnetClient.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace CodaxyTest.Services.NetflixConductor
{
    interface INetflixConductorCommunicationService
    {
        Task<bool> CreateTask(IWorkflowTask netflixRequestDto);
        Task<bool> CreateTask(Collection<IWorkflowTask> netflixRequestDtos);
        Task<bool> CreteWorkflow(NetflixConductorWorkflowDefinition workflowDefinition);
        Task<string> StartWorkflow(object data);
    }
}
